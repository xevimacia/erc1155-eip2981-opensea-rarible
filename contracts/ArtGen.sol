// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
// import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Burnable.sol";
import "@openzeppelin/contracts/interfaces/IERC2981.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
// Using math even though solidity 0.8.0 does not require SafeMath
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

import "./utils/ContextMixin.sol";
import "./utils/NativeMetaTransaction.sol";

/**
 * @title ERC1155ArtGen
 * ArtGen - ERC1155 contract that whitelists an operator address, has create and mint functionality, and supports useful standards from OpenZeppelin,
  like _exists(), name(), symbol(), and totalSupply()
 * It also has Pausable and Royalties function
 */
contract ArtGen is
    ContextMixin,
    ERC1155,
    NativeMetaTransaction,
    IERC2981,
    Ownable,
    Pausable
{
    using Strings for uint256;
    using SafeMath for uint256;

    address proxyRegistryAddress;
    uint256 private _currentTokenID = 0;
    mapping(uint256 => address) public creators;
    mapping(uint256 => uint256) public tokenSupply; // Number of minted tokens per tokenId
    mapping(uint256 => string) customUri; 
    // Contract name
    string public name;
    // Contract symbol
    string public symbol;
    // Variable used to max cap of tokens
    uint256 public total_supply;
    // Tokens global edition limit
    uint256 public edition_limit;
    // _recipient of the royalties
    address private _recipient;
    // percent of the royalties -> it is any value divided by 10000 -> 10% = 1000
    uint256 private _royaltyPercent;
    // Variable used to store the contract URI used by OpenSea
    string private contract_uri; // Format: "ipfs://bafkreigpykz4r3z37nw7bfqh7wvly4ann7woll3eg5256d2i5huc5wrrdq"

    /**
     * @dev Require _msgSender() to be the creator of the token id
     */
    modifier creatorOnly(uint256 _id) {
        require(
            creators[_id] == _msgSender(),
            "ERC1155ArtGen#creatorOnly: ONLY_CREATOR_ALLOWED"
        );
        _;
    }

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _uri,
        uint256 _total_supply,
        uint256 _edition_limit,
        uint256 _newRoyaltyPercent,
        string memory _contract_uri,
        address _proxyRegistryAddress // OpenSea proxy address: https://github.com/ProjectOpenSea/opensea-creatures/issues/116
    ) ERC1155(_uri) {
        name = _name;
        symbol = _symbol;
        total_supply = _total_supply;
        edition_limit = _edition_limit;
        contract_uri = _contract_uri;
        _recipient = owner();
        _royaltyPercent = _newRoyaltyPercent;
        proxyRegistryAddress = _proxyRegistryAddress;
        _initializeEIP712(_name);
    }

    event Minted(address _to, uint256 _tokenId, uint256 _quantity, string _tokenURI);

    /** @dev URI override for OpenSea traits compatibility.
    *   Keep customUri open for flexibility
    *   if no customUri, use json files in IPFS with base uri + token_id + .json 
    */
    function uri(uint256 _id) public view override returns (string memory) {
        // Tokens not in the record, were not minted so they do not exist
        require(_exists(_id), "ERC1155ArtGen#uri: URI query for nonexistent token");
        // We have to convert string to bytes to check for existence
        bytes memory customUriBytes = bytes(customUri[_id]);
        if (customUriBytes.length > 0) {
            return customUri[_id];
        } else {
            return string(abi.encodePacked(super.uri(_id), Strings.toString(_id)));
        }
    }

    /** @dev Contract-level metadata for OpenSea.
    * NOTE: Update for collection-specific metadata.
    * NOTE: Upon importing the contract to OpenSea, contract-level metadata
    *       will now pre-populate in the applicable collection fields.
    * NOTE: You can override collection details in OpenSea as desired.
    * NOTE: This value can be hardcoded instead of passed during contract deployment
    */
    function contractURI() public view returns (string memory) {
        return contract_uri; // Contract-level metadata for ArtGen
    }

    /**
     * @dev Returns the total quantity for a token ID
     * @param _id uint256 ID of the token to query
     * @return maximum_amount of tokens
     */
    function totalSupply(uint256 _id) public view returns (uint256) {
         // Tokens not in the record, were not minted so they do not exist
        require(_exists(_id), "ERC1155ArtGen#totalSupply: totalSupply query for nonexistent token");
        return tokenSupply[_id];
    }

    /**
     * @dev Returns the current token ID
     * @return _currentTokenID uint256 ID of the tokens
     * NOTE: done by me
     */
    function tokenID() public view returns (uint256) {
        return _currentTokenID;
    }

    /**
     * @dev Sets a new URI for all token types, by relying on the token type ID
     * substitution mechanism
     * https://eips.ethereum.org/EIPS/eip-1155#metadata[defined in the EIP].
     * @param _newURI New URI for all tokens
     */
    function setURI(string memory _newURI) public onlyOwner {
        _setURI(_newURI);
    }

    /**
     * @dev Will update the base URI for the token
     * @param _tokenId The token to update. _msgSender() must be its creator.
     * @param _newURI New URI for the token.
     */
    function setCustomURI(uint256 _tokenId, string memory _newURI)
        public
        creatorOnly(_tokenId)
    {
        customUri[_tokenId] = _newURI;
        emit URI(_newURI, _tokenId);
    }

    /**
     * @dev Creates a new token type and assigns _initialSupply to an address
     * NOTE: remove onlyOwner if you want third parties to create new tokens on
     *       your contract (which may change your IDs)
     * NOTE: The token id must be passed. This allows lazy creation of tokens or
     *       creating NFTs by setting the id's high bits with the method
     *       described in ERC1155 or to use ids representing values other than
     *       successive small integers. If you wish to create ids as successive
     *       small integers you can either subclass this class to count onchain
     *       or maintain the offchain cache of identifiers recommended in
     *       ERC1155 and calculate successive ids from that.
     * @param _initialOwner address of the first owner of the token
     * @param _initialSupply amount to supply the first owner
     * @param _uri Optional URI for this token type - Only send with one NFT
     * @param _data Data to pass if receiver is contract
     * @return The newly created token ID
     */
    function create(
        address _initialOwner,
        uint256 _initialSupply,
        string calldata _uri,
        bytes calldata _data
    ) public onlyOwner returns (uint256) {
        // Check if token id is greater than total_supply
        require(_initialSupply <= edition_limit, "ERC1155ArtGen#create: exceeded edition limit");
        // Manage the token id generation
        uint256 _id = _getNextTokenID();
        creators[_id] = _msgSender();

        if (bytes(_uri).length > 0) {
            customUri[_id] = _uri;
            emit URI(_uri, _id);
        }

        _mint(_initialOwner, _id, _initialSupply, _data);
        emit Minted(_msgSender(), _id, _initialSupply, uri(_id));

        tokenSupply[_id] = _initialSupply;

        return _id;
    }

    /**
     * @dev Mints some amount of tokens to an address
     * @param _to          Address of the future owner of the token
     * @param _id          Token ID to mint
     * @param _quantity    Amount of tokens to mint
     * @param _data        Data to pass if receiver is contract
     */
    function mint(
        address _to,
        uint256 _id,
        uint256 _quantity,
        bytes memory _data
    ) public virtual creatorOnly(_id) {
        require(_exists(_id), "ERC1155ArtGen#Mint: token _id does not exist");
        // Caps per token supply to x editions
        require((tokenSupply[_id] + _quantity) <= edition_limit, "ERC1155ArtGen#Mint: exceeded token maximum editions");
        _mint(_to, _id, _quantity, _data);
        emit Minted(_msgSender(), _id, _quantity, uri(_id));
        tokenSupply[_id] += _quantity;
    }

    /**
     * @dev Mint tokens for each id in _ids
     * @param _to          The address to mint tokens to
     * @param _ids         Array of ids to mint
     * @param _quantities  Array of amounts of tokens to mint per id
     * @param _data        Data to pass if receiver is contract
     */
    function batchMint(
        address _to,
        uint256[] memory _ids,
        uint256[] memory _quantities,
        bytes memory _data
    ) public {
        for (uint256 i = 0; i < _ids.length; i++) {
            require(
                _exists(_ids[i]),
                "ERC1155ArtGen#batchMint: token _id does not exist"
            );
            require(
                creators[_ids[i]] == _msgSender(),
                "ERC1155ArtGen#batchMint: ONLY_CREATOR_ALLOWED"
            );
            // Caps per token supply to x editions
            require((tokenSupply[_ids[i]] + _quantities[i]) <= edition_limit, "ERC1155ArtGen#batchMint: exceeded token maximum editions");
            tokenSupply[_ids[i]] += _quantities[i];
            // Emit token minted
            emit Minted(_msgSender(), _ids[i], _quantities[i], uri(_ids[i]));
        }
        _mintBatch(_to, _ids, _quantities, _data);
    }

    /**
     * @dev Returns whether the specified token exists by checking to see if it has a creator
     * @param _id uint256 ID of the token to query the existence of
     * @return bool whether the token exists
     */
    function _exists(uint256 _id) internal view returns (bool) {
        return creators[_id] != address(0);
    }
    function exists(uint256 _id) external view returns (bool) {
        return _exists(_id);
    }

    /**
     * @dev calculates the next token ID based on value of _currentTokenID
     * @return uint256 for the next token ID
     */
    function _getNextTokenID() internal returns (uint256) {
        // Check if token id is greater than total_supply
        require(_currentTokenID < total_supply, "ERC1155ArtGen#create: exceeded maximum token ids");
        _currentTokenID = _currentTokenID + 1;
        return _currentTokenID;
    }

    /**
     * @dev Change the creator address for given token
     * @param _to   Address of the new creator
     * @param _id  Token IDs to change creator of
     */
    function _setCreator(address _to, uint256 _id) internal creatorOnly(_id) {
        creators[_id] = _to;
    }
    /**
     * @dev Change the creator address for given tokens
     * @param _to   Address of the new creator
     * @param _ids  Array of Token IDs to change creator
     */
    function setCreator(address _to, uint256[] memory _ids) public {
        require(
            _to != address(0),
            "ERC1155ArtGen#setCreator: INVALID_ADDRESS."
        );
        for (uint256 i = 0; i < _ids.length; i++) {
            uint256 id = _ids[i];
            _setCreator(_to, id);
        }
    }

    /**
     * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-free listings.
     */
    // Won't use proxy and will have to copy the ERC1155.sol contract locally to edit the conditions for this function
    function isApprovedForAll(address _owner, address _operator)
        public
        view
        override
        returns (bool isOperator)
    {
        // Whitelist OpenSea proxy contract for easy trading.
        if (_operator == address(proxyRegistryAddress)) {
            return true;
        }
        return ERC1155.isApprovedForAll(_owner, _operator);
    }

    /**
     * @dev See {IERC1155-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) public virtual override {
        _setApprovalForAll(_msgSender(), operator, approved);
    }

    /**
    * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
    **/
    function _msgSender() internal view override returns (address sender) {
        return ContextMixin.msgSender();
    }

    /**
    * Pause and unpause the smart contract
    * Do not allow tokens transfer
    **/
    function pause() public onlyOwner {
        _pause();
    }
    function unpause() public onlyOwner {
        _unpause();
    }
    /**
    * Do not allow token transfer when paused
    * Override of function located in ERC1155 and is checked at transfer functions
    **/
    function _beforeTokenTransfer(address operator, address from, address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        internal
        whenNotPaused
        override
    {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }

    /** @dev EIP2981 royalties implementation. */

    // Maintain flexibility to modify royalties _recipient and basis points.
    function _setRoyalties(address newRecipient, uint256 newRoyaltyPercent) internal {
        // require(
        //     newRecipient != address(0),
        //     "Royalties: new recipient is the zero address"
        // );
        if (newRecipient != address(0)) {
            _recipient = newRecipient;
        }
        if (newRoyaltyPercent > 0) {
            _royaltyPercent = newRoyaltyPercent;
        }
    }
    function setRoyalties(address newRecipient, uint256 newRoyaltyPercent) external onlyOwner {
        _setRoyalties(newRecipient, newRoyaltyPercent);
    }
    
    // EIP2981 standard royalties return.
    function royaltyInfo(uint256 _tokenId, uint256 _salePrice)
        external
        view
        override
        returns (address receiver, uint256 royaltyAmount)
    {
        require(_exists(_tokenId), "ERC1155ArtGen#royaltyInfo: royaltyInfo query for nonexistent token");
        return (_recipient, (_salePrice * _royaltyPercent) / 10000);
    }

    // EIP2981 standard Interface return. Adds to ERC1155 and ERC165 Interface returns.
    // https://eips.ethereum.org/EIPS/eip-2981
    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC1155, IERC165)
        returns (bool)
    {
        return (interfaceId == type(IERC2981).interfaceId ||
            super.supportsInterface(interfaceId));
    }
}
