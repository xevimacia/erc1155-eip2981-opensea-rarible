# Full ERC1155 smartcontract with IPFS images, json attributes, and compatibility with Opensea and Rarible

This is a project that you can use to deploy your smart contract ERC1155 developed in Solidity.

The project has ideas taken from the following great resources:
1. [Openzeppelin ERC1155](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC1155/ERC1155.sol)
2. [Opensea creatures](https://github.com/ProjectOpenSea/opensea-creatures)
3. [Opensea metadata standards](https://docs.opensea.io/docs/metadata-standards)
4. [ERC1155 with EIP2981 for OpenSea](https://github.com/alxrnz2/ERC1155-with-EIP2981-for-OpenSea)

The smart contract is tested and deployed with Brownie - perfect for python developers!

The project has two smart contract deployments, you can check it out in the links below:
- ERC1155 NFT - universe
  - [OpenSea](https://testnets.opensea.io/collection/nft-universe-v1)
  - [Rarible](https://testnet.rarible.com/collection/0x871a092a3097181abcff176d15610d85430427b7/items)
- ERC1155 Semi-fungible - universe
  - [OpenSea](https://testnets.opensea.io/collection/semi-fungible-token-universe)
  - [Rarible](https://testnet.rarible.com/collection/0x96ee894f84c2291ba6e1a94efa2458437f663a62/items)
___
## Why may you use this repo?

The repo contains [OpenZeppelin](https://docs.openzeppelin.com/contracts/3.x/erc1155)'s standard [ERC1155](https://eips.ethereum.org/EIPS/eip-1155) contracts, which are modified for:

1. [EIP2981](https://eips.ethereum.org/EIPS/eip-2981) royalties standard for forward compatibility.The standard's royaltyInfo function returns a public address for the intended royalty recipient and a royalty amount in the sale currency. An exchange would query the function with the NFT's tokenId and sale price, and then remit royalties accordingly.

    `NOTE:`Since OpenSea doesn't currently support EIP2981 (as of August 2022), you'll need to manually enter royalty information. It works well in Rarible marketplace.
   
2. If you're looking to enable OpenSea listing (for primary or secondary sales), they recommend a few additions to OpenZeppelin's standard contracts to streamline integration:
- [Whitelisting](https://docs.opensea.io/docs/contract-level-metadata) via `isApprovedForAll` the OpenSea proxy contract address enables NFT buyers to list on OpenSea without paying gas fees.
- [Meta-transactions](https://docs.opensea.io/docs/contract-level-metadata) via `ContextMixin` enable gasless user transactions.
- (Not implemented in this contract) `PermanentURI` signals frozen token metadata.
- An override of the `uri` metadata function ensures OpenSea correctly caches token metadata/images without needing to rely on the ERC1155 ID substitution format.
- [contract-level metadata](https://docs.opensea.io/docs/contract-level-metadata) pre-populates basic information about the collection upon import.

    `Note:` These implementations are based on OpenSea support docs that might be outdated. But even if not required with OpenSea's latest releases, at a minimum, the changes shouldn't interfere with your listing.

3. Hard caps: If your token strategy relies on scarcity, you'll likely want to include caps in your contracts to reassure potential buyers. The examples contracts in this repo effectively cap tokens/editions at:
      
        NFT:
          nine/one, respectively.
        Semi-fungible Tokens:
          nine/ten respectively.
___
## Installation

If you are cloning the project then run this first, otherwise you can download the source code on the release page and skip this step.

```bash
git clone git@gitlab.com:xevimacia/erc1155-eip2981-opensea-rarible.git
```
Also create a `.env` file with the following variables:
```Python
# metamask test account keys
export PRIVATE_KEY={Your_key}
# Etherscan api key
export ETHERSCAN_TOKEN={your_API_key}
# To correct Error HH604: Error running JSON-RPC server: error:0308010C:digital envelope routines::unsupported
export NODE_OPTIONS=--openssl-legacy-provider
```
___
## How to handle the images and attributes?
Check my repo [json-ipfs-pin](https://gitlab.com/xevimacia/json-ipfs-pin) to  upload your images to IPFS and create the attribute json files to IPFS. All compatible with Opensea and Rarible.
___
## Packages
This project uses the following packages - most of them ready available with Python 3.8 and above:
[brownie](https://eth-brownie.readthedocs.io/en/stable/), [pytest](https://docs.pytest.org/en/7.1.x/), [requests](https://requests.readthedocs.io/en/latest/) and [time](https://docs.python.org/3/library/time.html)
___
## Usage
You can quickly test and deploy the project as it is. The project will use the json and images files available in IPFS.

To quickly fire up the project you can use either [Ganache](https://trufflesuite.com/ganache/) or [Hardhat](https://hardhat.org/) for the local chain.

-To compile the smart contract:
```bash
brownie compile
```
and check for any compliation errors

-To test:
```bash
brownie test --network hardhat
```
to run all available tests (integration and unit tests)
or
```bash
brownie test tests/unit/test_nft.py --network hardhat
```
to run only test_nft.py tests

-To locally deploy:
```bash
brownie run scripts/nft_universe.py --network hardhat
```
to deploy the ERC1155 NFT tokens
```bash
brownie run scripts/sft_universe.py --network hardhat
```
to deploy the ERC1155 Semi fungible tokens

-To deploy in rinkeby testnet:
```bash
brownie run scripts/nft_universe.py --network rinkeby
```
to deploy the ERC1155 NFT tokens
```bash
brownie run scripts/sft_universe.py --network rinkeby
```
to deploy the ERC1155 Semi fungible tokens

`NOTE: To deploy in ethereum main net change the network to mainnet`

These smartcontract gets automatically detected in both Opensea and Rarible marketplaces, under deployment the wallet address

Otherwise, if you wish to use it for your own NFTs, below are the steps you should follow to use this code:

1. Upload your images to IPFS and generate/upload the json and attributes using my repo [json-ipfs-pin](https://gitlab.com/xevimacia/json-ipfs-pin)

2. Decide if you want to create NFT or semi fungible tokens and edit the token parameters:
- NFT file to edit [nft_universe.py](./scripts/nft_universe.py).
- SFT file to edit [sft_universe.py](./scripts/sft_universe.py).
Edit the parameters below: 
```python
'''
Parameters used for the NFT smart contract deployment.
Parameters to be edited manually.
'''
AMOUNT_TO_MINT = 9  # total 9 NFTs corresponding to the 9 json files in IPFS
EDITION_LIMIT = 1  # NFT is 1/1
PIN = "QmedP3e2nsozbBQbPvXf1BuwwYDJM6khGCF6nz6gjzeaxN"  # json files IPFS pin
# parameters to be provided when calling the deploy function
name="NFT universe"
symbol="ONEUNI"
royalty_percent=1500
```
4. Deploy and pre-mint (or not) the smart contract by running the commands shared above.

At this stage you should be able to view your nft collection under your wallet address in opensea or rarible.

If you have issues to view your json or images in opensea. You can do the following:

5. Check if the os nft uri is valid for opensea
```bash
brownie run scripts/check_os_nft_uri.py --network hardhat
```
6. Verify smart contracts with Hardhat

In brownie, it is very easy to verify smart contracts. In the deploy function, we add the following argument:
```Python
deploy(..., publish_source=True)
```
Remember you have to create an etherscan account, generate the API key under [API keys](https://etherscan.io/myapikey) and add them to your `.env` file:
```Python
export ETHERSCAN_TOKEN={your_API_key}
```
___
## Photo Credits

These amazing artworks have been crafted by Juan Francisco Rodriguez Garcia.

You can follow him at:

[Instagram](https://www.instagram.com/juanrg92/)

[Behance](https://www.behance.net/juanrg92)

Each NFT json file has an external url corresponding to the creator of these amazing artworks.
___
## Writen in

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

and

![Solidity](https://img.shields.io/badge/Solidity-%23363636.svg?style=for-the-badge&logo=solidity&logoColor=white)

___
## License
[MIT](https://choosealicense.com/licenses/mit/)