from scripts.helpful_scripts import get_account, get_event_value, LOCAL_BLOCKCHAIN_ENVIRONMENTS
from scripts.deploy import deploy
from brownie import network, accounts
import pytest

"""
pytest global parameters
"""
@pytest.fixture(autouse=True)
def deploy_setup(request):
    return {"pin": "QmYPPTs6LWNiFApqa1a7kqcRuMJRyCvEhGvsEeu8x5MJAk", "royalty_percent": 1000, "AMOUNT_TO_MINT": 9}


def test_nft_brownie_collection(deploy_setup):
    """"""
    # Arrange.------------------------------------------------------------------
    account = get_account()
    """
    Deploy function
    Deploy the smart contract with total_supply = AMOUNT_TO_MINT (maximum number of token ids)
    and edition_limit = 10 (maximum number of tokens per token id)
    @params:
        name: name of the collection,
        symbol: symbol of the collection,
        pin: IPFS pin used to generate the base uri and contract_uri
        total_supply: maximum number of token id
        edition_limit: maximum number of tokens minted per token id
        newRoyaltyPercent: % of royalties to the creator by x / 10000 -> 10% = 1000
    """
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    nft = deploy(pin=deploy_setup["pin"], total_supply=deploy_setup["AMOUNT_TO_MINT"], edition_limit=10)
    
    # Act.----------------------------------------------------------------------
    # Create each token id. No initial_supply (=minted tokens yet)
    """
    Create function
    @params:
        initialOwner: use the creator's account
        initialSupply: How many initial tokens to be minted - cannot be higher than edition_limit
        uri: Send the base uri, if needed or empty
        data: empty
    """
    id_count = 0
    while id_count < deploy_setup["AMOUNT_TO_MINT"]:
        id_count = id_count + 1
        one_token_uri = ""
        nft.create(account, 0, one_token_uri,
                    "", {"from": account})  # initial_supply = 0 token for all
    
    # We are going to mint tokens 1, 2, 3, 4 and 5 as NFTs (only edition of 1/1)
    # Tokens 6 and 7 with edition of 5/5
    # Tokens 8 and 9 with edition of 10/10 (edition limit)
    """
    Mint tokens
    @params:
        to: new owner address
        id: token id
        quantity: how many tokens minted
        data: empty
    """
    id_count = 0
    tx = None
    while id_count < deploy_setup["AMOUNT_TO_MINT"]:
        id_count = id_count + 1
        # mint NFTs for token_id 1, 2, 3, 4 and 5 (only edition of 1/1)
        if id_count <= 5:
            tx = nft.mint(account, id_count, 1, "", {"from": account})
        # mint 5 tokens for each token_id 6 and 7 (5/5)
        elif id_count <= 7:
            tx = nft.mint(account,id_count, 5, "", {"from": account})
        # mint 10 tokens for each token_id 8 and 9 (10 tokens is the edition limit)
        else:
            tx = nft.mint(account,id_count, 10, "", {"from": account})

    # Assert.-------------------------------------------------------------------
    assert nft.name() == 'Art Gen NFT'
    assert nft.symbol() == 'ARTGENFT'
    assert nft.tokenID() == deploy_setup["AMOUNT_TO_MINT"]

    assert get_event_value(tx, "Minted", "_tokenId") == nft.tokenID()
    assert get_event_value(tx, "Minted", "_to") == account.address
    assert get_event_value(
        tx, "Minted", "_tokenURI") == nft.uri(nft.tokenID())

    assert nft.contractURI().startswith("ipfs://")

    assert nft.exists(nft.tokenID() + 1) is not True
    assert nft.exists(nft.tokenID()) is True

    assert nft.balanceOf(account, nft.tokenID()) == 10
    assert nft.balanceOf(accounts[1], nft.tokenID()) == 0

    assert nft.uri(nft.tokenID()).startswith("https://ipfs.io/ipfs/")

    # 0.1 ETH
    sale_price = 0.1
    addr, amount = nft.royaltyInfo(nft.tokenID(), sale_price)
    assert addr == account.address and amount == sale_price * deploy_setup["royalty_percent"] / 10000
