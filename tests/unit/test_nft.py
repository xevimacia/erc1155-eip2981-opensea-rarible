from scripts.helpful_scripts import get_account, LOCAL_BLOCKCHAIN_ENVIRONMENTS
from scripts.deploy import deploy, rinkeby_os_proxy
from brownie import network, exceptions, accounts
import pytest

"""
pytest global parameters
"""
@pytest.fixture(autouse=True)
def deploy_setup(request):
    return {"pin": "QmYPPTs6LWNiFApqa1a7kqcRuMJRyCvEhGvsEeu8x5MJAk", "royalty_percent": 1000}


"""
Deploy function
@params:
    name: name of the collection,
    symbol: symbol of the collection,
    pin: IPFS pin used to generate the base uri and contract_uri
    total_supply: maximum number of token id
    edition_limit: maximum number of tokens minted per token id
    newRoyaltyPercent: % of royalties to the creator by x / 10000 -> 10% = 1000
"""
def test_can_deploy(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()


"""
Create function
@params:
    initialOwner: use the creator's account
    initialSupply: How many initial tokens to be minted - cannot be higher than edition_limit
    uri: Send the base uri, if needed or empty
    data: empty
"""
def test_create(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract
    # we can have 2 token ids and the each token id has a limit of 10 editions
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=2, edition_limit=10)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token 1
    # one_token_uri = f"https://ipfs.io/ipfs/{pin}/{str(art_gen.tokenID()+1)}"
    one_token_uri = ""
    art_gen.create(get_account(), 1, one_token_uri,
                   "", {"from": get_account()})  # initial_supply = 1 token
    # check that the token type 1 exists
    assert art_gen.exists(art_gen.tokenID()) == True
    # check that token id is 1
    assert art_gen.tokenID() == 1
    # check the balance of token type 1
    assert art_gen.totalSupply(art_gen.tokenID()) == 1
    # Check we cannot create a new token with initial_supply above 10 (edition_limit)
    with pytest.raises(exceptions.VirtualMachineError):
        # Create new token 2
        art_gen.create(get_account(), 20, one_token_uri,
                       "", {"from": get_account()})  # initial supply = 20 token
    # Create a new token 2
    art_gen.create(get_account(), 10, one_token_uri,
                   "", {"from": get_account()})  # initial_supply = 10 token
    # check that the token type 2 exists
    assert art_gen.exists(art_gen.tokenID()) == True
    # check that token id is 2
    assert art_gen.tokenID() == 2
    # check the balance of token type 2
    assert art_gen.totalSupply(art_gen.tokenID()) == 10
    # check that we cannot create a new token as total_supply = 2.
    with pytest.raises(exceptions.VirtualMachineError):
        # Create a new token 3
        art_gen.create(get_account(), 0, one_token_uri,
                       "", {"from": get_account()})  # initial supply = 0 token


"""
uri management function
"""
def test_uri(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract with pin
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=2, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Check if contractURI is set
    assert art_gen.contractURI() == f'ipfs://{deploy_setup["pin"]}'
    # Create a new token with initial_supply = 0. Not minted
    one_token_uri = ""
    art_gen.create(get_account(), 0, one_token_uri, "")
    # Check if base uri is set
    assert art_gen.uri(art_gen.tokenID(
    )) == f'https://ipfs.io/ipfs/{deploy_setup["pin"]}/{art_gen.tokenID()}.json'
    # check that we cannot create a new token as total_supply = 2.
    with pytest.raises(exceptions.VirtualMachineError):
        # Get uri of not existing token id 2
        art_gen.uri(2)  # Token ID does not exist
    # change the uri base using a new dummy pin
    new_pin = "dummy_pin"
    art_gen.setURI(f'https://ipfs.io/ipfs/{new_pin}/')
    assert art_gen.uri(
        art_gen.tokenID()) == f'https://ipfs.io/ipfs/{new_pin}/{art_gen.tokenID()}.json'
    # customize token at token creation
    custom_uri = "https://ipfs.io/ipfs/this_is_a_custom_uri/custom.json"
    art_gen.create(get_account(), 0, custom_uri, "")
    # Check if base uri is set
    assert art_gen.uri(art_gen.tokenID()) == custom_uri
    # change the custom uri in case of updates
    change_uri = "https://ipfs.io/ipfs/change_custom_uri/change.json"
    art_gen.setCustomURI(art_gen.tokenID(), change_uri)
    assert art_gen.uri(art_gen.tokenID()) == change_uri
    # set permanent_uri - not implemented in the smartcontract
    # In ArtGen.sol, add the PermanentURI in create, mint and batchMint (in the for loop). Example below:
    # emit PermanentURI(string(abi.encodePacked(_uriBase, Strings.toString(id), ".json")), id);

"""
Mint a new token
@params:
    to: new owner address
    id: token id
    quantity: how many tokens minted
    data: empty
"""
def test_mint(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. edition_limit = 1 equivalent to an NFT
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token with initial_supply = 0. Token not minted
    one_token_uri = ""
    art_gen.create(get_account(), 0, one_token_uri, "", {"from": get_account()})
    # check that the new token id exists
    assert art_gen.exists(art_gen.tokenID()) == True
    assert art_gen.tokenID() == 1
    # Mint a new token
    # check that we cannot mint a non-existing token id
    with pytest.raises(exceptions.VirtualMachineError):
        # mint 1 token with token id = 2 should fail.
        art_gen.mint(get_account(), 2, 1, "", {"from": get_account()})
    # check that we have created an NFT = Cannot mint more than one token
    with pytest.raises(exceptions.VirtualMachineError):
        # Mint 2 tokens with edition_limit = 1 should fail
        art_gen.mint(get_account(), art_gen.tokenID(), 2, "", {"from": get_account()})
    # Mint 1 token to create an NFT
    art_gen.mint(get_account(), art_gen.tokenID(), 1, "", {"from": get_account()})
    # check the balance of token type
    assert art_gen.totalSupply(art_gen.tokenID()) == 1
    # check that the uri is created using uri_base
    assert art_gen.uri(art_gen.tokenID(
    )) == f'https://ipfs.io/ipfs/{deploy_setup["pin"]}/{art_gen.tokenID()}.json'


"""
Mint a batch of new tokens
@params:
    to: new owner address
    ids: array of token ids
    quantities: array of quantities
    data: empty
"""
def test_batch_mint(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. Max 3 token types with a limit of 10 editions
    total_supply=3
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=total_supply, edition_limit=10)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a 3 new tokens with initial supply = 0
    token_ids = []
    for token_id in range(1, total_supply+1):
        one_token_uri = ""
        art_gen.create(get_account(), 0, one_token_uri, "", {"from": get_account()})
        # check that the new token id exists
        assert art_gen.exists(art_gen.tokenID()) == True
        assert art_gen.tokenID() == token_id
        token_ids.append(token_id)
    # Mint a batch of new tokens
    # check that we cannot mint a non-existing token id
    with pytest.raises(exceptions.VirtualMachineError):
        # mint 1 token with token id = 4 should fail.
        token_ids_fail = [1, 2, 3, 4]
        token_quantities = [1, 1, 1, 1]
        art_gen.batchMint(get_account(), token_ids_fail, token_quantities, "", {"from": get_account()})
    # check that we have created an NFT = Cannot mint more than 10 tokens
    with pytest.raises(exceptions.VirtualMachineError):
        # Mint one of the new tokens with edition_limit = 11 should fail
        token_quantities = [10, 10, 11]
        art_gen.batchMint(get_account(), token_ids, token_quantities, "", {"from": get_account()})
    # Mint the batch of 3 tokens
    token_quantities = [10, 10, 10]
    art_gen.batchMint(get_account(), token_ids, token_quantities, "", {"from": get_account()})
    # check the balance per token type and the uri is created using uri_base
    for token_id in range(1, total_supply+1):
        assert art_gen.totalSupply(token_id) == 10
        assert art_gen.uri(token_id) == f'https://ipfs.io/ipfs/{deploy_setup["pin"]}/{token_id}.json'

"""
Transfer token to another account with safeTransferFrom
In this case, opensea will be the one managing the sales.
Opensea smartcontract will make sure ERC20 payment is complete and call this transfer function
In your marketplace, you need to create a new smartcontract that inherits ArtGen to process the sale.
In this case, the opensea standard called Seaport could be used:
https://github.com/ProjectOpenSea/seaport/blob/main/docs/SeaportDocumentation.md
@params:
    from: owner's address
    to: new address to transfer the token
    id: token id
    amount: quantity of tokens transferred
    data: empty
"""
def test_transfer(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. edition_limit = 1 equivalent to an NFT
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token with initial_supply = 1. Token not minted
    one_token_uri = ""
    art_gen.create(get_account(), 0, one_token_uri, "", {"from": get_account()})
    # check that the new token id exists
    assert art_gen.exists(art_gen.tokenID()) == True
    assert art_gen.tokenID() == 1
    # Check that cannot transfer a non existing token_id
    with pytest.raises(exceptions.VirtualMachineError):
        # we use token_id = 10
        art_gen.safeTransferFrom(get_account(), accounts[1], 10, 1, "", {"from": get_account()})
    # Check that cannot transfer not minted token
    with pytest.raises(exceptions.VirtualMachineError):
        art_gen.safeTransferFrom(get_account(), accounts[1], art_gen.tokenID(), 1, "", {"from": get_account()})
    # Mint 1 token to create an NFT
    art_gen.mint(get_account(), art_gen.tokenID(), 1, "", {"from": get_account()})
    # check the balance of token type
    assert art_gen.totalSupply(art_gen.tokenID()) == 1
    # minting account should have 1 token of type 1
    assert art_gen.balanceOf(get_account(), art_gen.tokenID()) == 1 
    # Check that cannot transfer a quantity bigger than 1 minted token
    with pytest.raises(exceptions.VirtualMachineError):
        # instead of 1 minted token, try to transfer 5
        art_gen.safeTransferFrom(get_account(), accounts[1], art_gen.tokenID(), 5, "", {"from": get_account()})
    # transfer the 1 minted token to a new address
    # owner can transfer as from == _msgSender() (Solidity condition)
    art_gen.safeTransferFrom(get_account(), accounts[1], art_gen.tokenID(), 1, "", {"from": get_account()})
    # minting account should have 0 tokens of type 1
    assert art_gen.balanceOf(get_account(), art_gen.tokenID()) == 0
    # new account should have 1 token of type 1
    assert art_gen.balanceOf(accounts[1], art_gen.tokenID()) == 1
    # transfer the NFT to a second account[2] through proxy opensea marketplace or whichever is whitelisted
    # in the test it is rinkeby_os_proxy located in deploy.py
    art_gen.safeTransferFrom(accounts[1], accounts[2], art_gen.tokenID(), 1, "", {"from": accounts[1]})
    # account[1] account should have 0 tokens of type 1
    assert art_gen.balanceOf(accounts[1], art_gen.tokenID()) == 0
    # account[2] should have 1 token of type 1
    assert art_gen.balanceOf(accounts[2], art_gen.tokenID()) == 1
    # accounts[2] whitelist the marketplace for transfer tokens
    # using rinkeby_os_proxy located in deploy.py
    art_gen.setApprovalForAll(rinkeby_os_proxy, True, {"from": accounts[2]})
    assert art_gen.isApprovedForAll(accounts[2], rinkeby_os_proxy) == True
    # transfer the NFT from accounts[2] to account[1] through proxy opensea marketplace or whichever market is whitelisted
    # in our case rinkeby_os_proxy
    art_gen.safeTransferFrom(accounts[2], accounts[1], art_gen.tokenID(), 1, "", {"from": rinkeby_os_proxy})
    # account[2] should have 0 tokens of type 1
    assert art_gen.balanceOf(accounts[2], art_gen.tokenID()) == 0
    # account[1] account should have 1 token of type 1
    assert art_gen.balanceOf(accounts[1], art_gen.tokenID()) == 1

"""
batch transfer of tokens to another account with safeBatchTransferFrom
In this case, opensea will be the one managing the sales. Check safeTransferFrom above for details
@params:
    from: owner's address
    to: new address to transfer the token
    ids: array of token ids
    amounts: array of quantities
    data: empty
"""
def test_batch_transfer(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. Max 3 token types with a limit of 10 editions
    total_supply=3
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=total_supply, edition_limit=10)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a 3 new tokens with initial supply = 0
    token_ids = []
    for token_id in range(1, total_supply+1):
        one_token_uri = ""
        art_gen.create(get_account(), 0, one_token_uri, "", {"from": get_account()})
        # check that the new token id exists
        assert art_gen.exists(art_gen.tokenID()) == True
        assert art_gen.tokenID() == token_id
        token_ids.append(token_id)
    # Check that cannot transfer a non existing token_id
    with pytest.raises(exceptions.VirtualMachineError):
        # we use token_ids = [1, 2, 3, 4]
        token_ids_fail = [1, 2, 3, 4]
        amounts = [1, 1, 1, 1]
        art_gen.safeBatchTransferFrom(get_account(), accounts[1], token_ids_fail, amounts, "", {"from": get_account()})
    # Check that cannot transfer not minted token
    with pytest.raises(exceptions.VirtualMachineError):
        # we use token_ids
        amounts = [1, 1, 1, 1]
        art_gen.safeBatchTransferFrom(get_account(), accounts[1], token_ids, amounts, "", {"from": get_account()})
    # Mint the batch of 3 tokens
    token_quantities = [10, 10, 10]
    art_gen.batchMint(get_account(), token_ids, token_quantities, "", {"from": get_account()})
    # check the balance per token type and the uri is created using uri_base
    for token_id in range(1, total_supply+1):
        assert art_gen.totalSupply(token_id) == 10
    # minting account should have balances of 10 in each token type
    minting_account = [get_account(),get_account(),get_account()]
    assert art_gen.balanceOfBatch(minting_account, token_ids) == [10,10,10]
    # Check that cannot transfer a quantity bigger than 10 minted tokens per type
    with pytest.raises(exceptions.VirtualMachineError):
        # instead of [10,10,10] minted tokens, try to transfer [11,11,11]
        token_quantities_fail = [11, 11, 11]
        art_gen.safeBatchTransferFrom(get_account(), accounts[1], token_ids, token_quantities_fail, "", {"from": get_account()})
    # transfer 4 tokens from minting_account of each token id to new account
    art_gen.safeBatchTransferFrom(get_account(), accounts[1], token_ids, [4,4,4], "", {"from": get_account()})
    # check that minting_account has 6 tokens in each token_id
    assert art_gen.balanceOfBatch(minting_account, token_ids) == [6,6,6]
    # check that account_with_four_tokens has 4 tokens in each token_id
    account_one = [accounts[1],accounts[1],accounts[1]]
    assert art_gen.balanceOfBatch(account_one, token_ids) == [4,4,4]
    # transfer 5 tokens from minting_account of each token id to new account
    art_gen.safeBatchTransferFrom(get_account(), accounts[2], token_ids, [5,5,5], "", {"from": get_account()})
    # check that minting_account has only 1 token in each token_id
    assert art_gen.balanceOfBatch(minting_account, token_ids) == [1,1,1]
    # check that account_with_four_tokens has 5 tokens in each token_id
    account_two = [accounts[2],accounts[2],accounts[2]]
    assert art_gen.balanceOfBatch(account_two, token_ids) == [5,5,5]
    # NOTE: FAILS HERE
    # transfer 5 tokens from account[2] of each token id to account[1]
    art_gen.safeBatchTransferFrom(accounts[2], accounts[1], token_ids, [5,5,5], "", {"from": accounts[2]})
    # check that accounts[2] has only 0 token in each token_id
    assert art_gen.balanceOfBatch(account_two, token_ids) == [0,0,0]
    # check that accounts[1] has 9 tokens in each token_id
    assert art_gen.balanceOfBatch(account_one, token_ids) == [9,9,9]
    # accounts[2] whitelist the marketplace for transfer tokens
    # using rinkeby_os_proxy located in deploy.py
    art_gen.setApprovalForAll(rinkeby_os_proxy, True, {"from": accounts[2]})
    assert art_gen.isApprovedForAll(accounts[2], rinkeby_os_proxy) == True
    # transfer 9 tokens from account[1] of each token id to account[2] through proxy opensea marketplace or whichever market is whitelisted
    # in our case rinkeby_os_proxy
    art_gen.safeBatchTransferFrom(accounts[1], accounts[2], token_ids, [9,9,9], "", {"from": rinkeby_os_proxy})
    # check that accounts[1] has only 0 token in each token_id
    assert art_gen.balanceOfBatch(account_one, token_ids) == [0,0,0]
    # check that accounts[2] has 9 tokens in each token_id
    assert art_gen.balanceOfBatch(account_two, token_ids) == [9,9,9]

"""
pause smart contract function
"""
def test_pause(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. edition_limit = 1 equivalent to an NFT
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token with initial_supply = 1. Token minted
    one_token_uri = ""
    art_gen.create(get_account(), 1, one_token_uri, "", {"from": get_account()})
    # check that the new token id exists
    assert art_gen.exists(art_gen.tokenID()) == True
    assert art_gen.tokenID() == 1
    # pause the smart contract
    art_gen.pause()
    assert art_gen.paused() == True
    # contract paused - check that we cannot transfer the newly minted token
    with pytest.raises(exceptions.VirtualMachineError):
        art_gen.safeTransferFrom(get_account(), accounts[1], art_gen.tokenID(), 1, "", {"from": get_account()})
    # unpause the smart contract
    art_gen.unpause()
    assert art_gen.paused() == False
    # transfer the 1 minted token to a new address
    # owner can transfer as from == _msgSender() (Solidity condition)
    art_gen.safeTransferFrom(get_account(), accounts[1], art_gen.tokenID(), 1, "", {"from": get_account()})
    # minting account should have 0 tokens of type 1
    assert art_gen.balanceOf(get_account(), art_gen.tokenID()) == 0
    # new account should have 1 token of type 1
    assert art_gen.balanceOf(accounts[1], art_gen.tokenID()) == 1

"""
set new creator function
"""
def test_new_creator(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. edition_limit = 1 equivalent to an NFT
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token with initial_supply = 1. Token not minted
    one_token_uri = ""
    art_gen.create(get_account(), 0, one_token_uri, "", {"from": get_account()})
    # check that the new token id exists
    assert art_gen.exists(art_gen.tokenID()) == True
    assert art_gen.tokenID() == 1
    # change creator of the token to accounts[1] and list of token_id
    art_gen.setCreator(accounts[1], [art_gen.tokenID()])
    # Mint a new token
    # check that we cannot mint with get_account() 
    with pytest.raises(exceptions.VirtualMachineError):
        # mint 1 token with token id = 2 should fail.
        art_gen.mint(get_account(), 1, 1, "", {"from": get_account()})
    # mint with accounts[1]
    art_gen.mint(accounts[1], 1, 1, "", {"from": accounts[1]})
    # check the balance of token type
    assert art_gen.totalSupply(art_gen.tokenID()) == 1
    # minting account should have 1 token of type 1
    assert art_gen.balanceOf(accounts[1], art_gen.tokenID()) == 1

"""
set royalties
Only if marketplace uses EIP2981 royalties implementation
https://eips.ethereum.org/EIPS/eip-2981
"""
def test_royalties(deploy_setup):
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Deploy the smartcontract. edition_limit = 1 equivalent to an NFT
    art_gen = deploy(pin=deploy_setup["pin"], total_supply=1, edition_limit=1)
    # Check that current account deployed smart contract
    assert art_gen.owner() == get_account()
    # Create a new token with initial_supply = 1. Token not minted
    one_token_uri = ""
    art_gen.create(get_account(), 1, one_token_uri, "", {"from": get_account()})
    # check that the new token id exists
    assert art_gen.exists(art_gen.tokenID()) == True
    assert art_gen.tokenID() == 1
    # check the balance of token type
    assert art_gen.totalSupply(art_gen.tokenID()) == 1
    # minting account should have 1 token of type 1
    assert art_gen.balanceOf(get_account(), art_gen.tokenID()) == 1
    # check if contract support royalites
    INTERFACE_ID_ERC2981 = 0x2a55205a # from https://eips.ethereum.org/EIPS/eip-2981
    assert art_gen.supportsInterface(INTERFACE_ID_ERC2981) == True
    # set royalties to 10% -> 1000/10000=10% -> from pytest global parameters
    art_gen.setRoyalties(get_account(), deploy_setup["royalty_percent"], {"from": get_account()})
    # check royalties to be paid and to whom
    # 0.1 ETH
    sale_price = 0.1
    recipient, amount = art_gen.royaltyInfo(art_gen.tokenID(), sale_price)
    assert recipient == get_account()
    assert amount == 0.1 * deploy_setup["royalty_percent"] / 10000
    recipient_zero, amount_zero = art_gen.royaltyInfo(art_gen.tokenID(), 0)
    assert recipient_zero == get_account()
    assert amount_zero == 0
