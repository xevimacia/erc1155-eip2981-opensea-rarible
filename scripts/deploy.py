
from brownie import ArtGen, network, config

from scripts.helpful_scripts import get_account, LOCAL_BLOCKCHAIN_ENVIRONMENTS

# Marketplace smart contract addresses
# Opensea ERC1155
# https://github.com/ProjectOpenSea/opensea-creatures/blob/master/migrations/2_deploy_contracts.js
# https://ethereum.stackexchange.com/questions/122237/why-opensea-polygon-proxy-contract-does-not-have-transactions
rinkeby_os_proxy = "0x1E525EEAF261cA41b809884CBDE9DD9E1619573A"
mainnet_os_proxy = "0xa5409ec958c83c3f309868babaca7c86dcb077c1"
polygon_os_proxy = "0x207Fa8Df3a17D96Ca7EA4f2893fcdCb78a304101"
# Rarible ERC1155
# https://docs.rarible.org/reference/contract-addresses/
rinkeby_rarible_proxy = "0x1AF7A7555263F275433c6Bb0b8FdCD231F89B1D7"
mainnet_rarible_proxy = "0xB66a603f4cFe17e3D27B87a8BfCaD319856518B8"

"""
Smart contract level arguments
@params:
    name: name of the collection,
    symbol: symbol of the collection,
    uri: base uri, can be changed at any time. Also can use custom uri for each token id
    total_supply: maximum number of token id
    edition_limit: maximum number of tokens minted per token id
    newRoyaltyPercent: % of royalties to the creator by x / 10000
    contract_uri: Upon importing the contract to OpenSea, contract-level metadata will now pre-populate in the applicable collection fields. 
    proxyRegistryAddress: Whitelisting the OpenSea proxy contract address enables NFT buyers to list on OpenSea without paying gas fees.
"""


def deploy(pin, total_supply, edition_limit, name='Art Gen NFT', symbol='ARTGENFT', royalty_percent=1000):
    # base uri
    base_uri = f"https://ipfs.io/ipfs/{pin}/" if pin else ""
    # Contract URI
    contract_uri = f"ipfs://{pin}" if pin else ""
    account = get_account()
    # publish_source=True to publish in etherscan
    # set the etherscan api token in .env
    # export ETHERSCAN_TOKEN=YourToken
    # more information here: https://matnad.medium.com/how-to-verify-your-brownie-project-on-etherscan-82d6086644cf
    art_gen = ArtGen.deploy(name, symbol, base_uri, total_supply, edition_limit,
                            royalty_percent, contract_uri, rinkeby_os_proxy,
                            {"from": account},
                            publish_source=config["networks"][network.show_active()].get("verify", False))
    return art_gen


def main():
    deploy()
