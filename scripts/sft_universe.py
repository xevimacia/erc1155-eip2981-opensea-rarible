
from brownie import network
from scripts.helpful_scripts import get_account, check_token_uri, LOCAL_BLOCKCHAIN_ENVIRONMENTS
from scripts.deploy import deploy

# Parameters used for the deployment
AMOUNT_TO_MINT = 9  # total 9 NFTs corresponding to the 9 json files in IPFS
EDITION_LIMIT = 10  # SFT (semi-fungible token) is a multiple number
PIN = "QmedP3e2nsozbBQbPvXf1BuwwYDJM6khGCF6nz6gjzeaxN"  # json files IPFS pin

def deploy_sft_universe():
    account = get_account()
    # Deploy the smartcontract with pin
    # if it is deployed in mainnet, change the proxy to mainnet in ./scripts/deploy.py
    sft = deploy(pin=PIN, total_supply=AMOUNT_TO_MINT, edition_limit=EDITION_LIMIT,
                     name="semi-fungible token universe", symbol="MULTIUNI", royalty_percent=1500)
    # create and mint all 9 SFTs
    id_count = 0
    while id_count < AMOUNT_TO_MINT:
        id_count = id_count + 1
        one_token_uri = ""
        sft.create(account, 10, one_token_uri,
                    "", {"from": account})  # initial_supply = 10 -> all minted
    print(f'uri token 1: {sft.uri(1)}')
    print(f'uri token 9: {sft.uri(9)}')
    # Do not verify if it is testing locally
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        check_token_uri(contract_address=sft.address, total_supply=AMOUNT_TO_MINT)
def main():
    deploy_sft_universe()
