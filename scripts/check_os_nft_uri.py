# token uri verification of in opensea testnet rinkeby
# REST api example
# https://testnets-api.opensea.io/asset/0xe428a0853cdc56ef647e08a2f30084ce3788aaa8/1/validate
# check docs for how to use https://docs.opensea.io/reference/validate-assets
import requests
import time

def check_nft_uri():
    headers = {"Accept": "application/json"}
    opensea_uri = "https://testnets-api.opensea.io/asset/"
    address = "0xAcCf56DC66080a29821259304410298fee18BFD0"
    token_id = 1
    while token_id <= 9:
        uri = f"{opensea_uri}{address}/{token_id}/validate"
        response = requests.get(uri, headers=headers)
        print(response.text)
        token_id = token_id + 1
        time.sleep(1) # wait 1 second to avoid error "Request was throttled. Expected available in 1 second"

def main():
    check_nft_uri()
